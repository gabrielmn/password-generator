/*
    This file is part of Password Generator.
    Copyright (C) 2022 Gabriel M. Nori

    Password Generator is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <time.h>   
#include <ctype.h>

// parameters
// -c --case-sensitive
// -l <LENGTH> --length <LENGTH> 
// -n --numbers
// -s --special-characters
//    --help
//    --version

const char letters[] = {
    'a',
    'b',
    'c',
    'd',
    'e',
    'f',
    'g',
    'h',
    'i',
    'j',
    'k',
    'l',
    'm',
    'n',
    'o',
    'p',
    'q',
    'r',
    's',
    't',
    'u',
    'v',
    'w',
    'x',
    'y',
    'z',
    '\0'
};

const char numbers[] = {
    '0',
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '\0'
};

const char special_characters[] = {
    '!',
    '@',
    '#',
    '$',
    '%',
    '(',
    ')',
    '?',
    '[',
    '{',
    ']',
    '}',
    '\0'
};

void print_help(){
    std::string help("");
    help.append("Usage: password-generator [OPTIONS]\n");
    help.append("Generate a password.\n");
    help.append("\n");
    help.append("Mandatory arguments to long options are mandatory for short options too.\n");
    help.append("  -c --case-sensitive\t\tenable upper case letter on the password\n");
    help.append("  -l --length=LENGTH\t\tset password length, default is 16\n");
    help.append("  -n --numbers\t\t\tenable numbers on the password\n");
    help.append("  -s --special-characters\tenable special characters on the password\n");
    help.append("\n");
    help.append("     --help\t\t\tdisplay this help and exit\n");
    help.append("     --version\t\t\toutput version information and exit\n");

    std::cout << help << std::endl;
}

void print_version(){
    std::string version("");
    version.append("password-generator 1.0.0\n");
    version.append("Copyright (C) 2022 Gabriel M. Nori\n");
    version.append("License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.\n");
    version.append("This is free software: you are free to change and redistribute it.\n");
    version.append("There is NO WARRANTY, to the extent permitted by law.\n\n");
    version.append("Written by Gabriel M. Nori.");

    std::cout << version << std::endl;
}

int main(int argc, char **argv) {

    bool enable_numbers = false;
    bool enable_case_sensitive = false;
    bool enable_special_characters = false;
    int length = 16;
    
    for (int i = 1; i < argc; i++)
    {
        if(argv[i][0] == '-'){
            // full name
            if(argv[i][1] == '-'){
                if( strcmp(argv[i], "--case-sensitive")  == 0 ){
                    enable_case_sensitive = true;                      
                }
                else if( strcmp(argv[i], "--length")  == 0 ){
                    try{
                        i++;
                        length = std::stoi(argv[i]);
                    }catch(std::exception const &e){
                        // TODO improve error msg
                        std::cout << "invalid length" << std::endl;
                    }
                }
                else if( strcmp(argv[i], "--numbers")  == 0 ){
                    enable_numbers = true;                      
                }
                else if( strcmp(argv[i], "--special-characters")  == 0 ){
                    enable_special_characters = true;                      
                }
                else if( strcmp(argv[i], "--help")  == 0 ){
                    print_help();
                    return 0;
                }    
                else if( strcmp(argv[i], "--version")  == 0 ){
                    print_version();
                    return 0;
                }
            }
            // short name
            else{
                for(size_t j = 1; j < strlen(argv[i]); j++){
                    if(argv[i][j] == 'c'){
                        enable_case_sensitive = true;
                    }
                    else if( argv[i][j] == 'l' ){
                        try{
                            i++;
                            length = std::stoi(argv[i]);
                            break;
                        }catch(std::exception const &e){
                            // TODO improve error msg
                            std::cout << "invalid length" << std::endl;
                        }
                    }   
                    else if (argv[i][j] == 's'){
                        enable_special_characters = true;
                    }
                    else if (argv[i][j] == 'n'){
                        enable_numbers = true;
                    }
                }
            }
        }
    } 
    // setup charset
    std::string charset(letters);
    if(enable_numbers){
        charset.append(numbers);
    }
    if(enable_special_characters){
        charset.append(special_characters);
    }
    // create password
    srand(time(NULL));
    std::string password("");
    for (int i = 0; i < length; i++) {
		char character = charset[rand() % charset.length()];
        if(enable_case_sensitive && isalpha(character) && (rand() % 2)){
			character = toupper(character);
		}
		password.push_back(character);
	}
    // print password 
    std::cout << password << std::endl;
}
