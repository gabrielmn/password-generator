# Password Generator

This project serves to generate random password, the passwords generate with this program can have variable length and also be a mix of lower case letters, upper case letters, numbers and special characters.

### Table of Contents

  - [Prerequisites](#prerequisites)
  - [Structure](#structure)
  - [Building](#building)
  - [Debug](#debug)
  - [Install](#install)
  - [Release](#release)
  - [Run](#run)
  - [Usage](#usage)
  - [Contributing](#contributing)
  - [License](#license)

## Prerequisites

This project requires the following items:

- make
- gcc
- mingw-w64-gcc
- wine(optional)

Make is used to generate the executables for linux and windows, make uses gcc to compile the linux executable and mingw-w64-gcc to compile the windows executable. Wine can be installed to test the windows executable on linux.

### Installation

On Arch Linux:

The following command can be used to install make.
```shell
pacman -S --needed make
```
The following command can be used to install g++.
```shell
pacman -S --needed gcc
```
The following command can be used to install wine.
```shell
pacman -S --needed mingw-w64-gcc
```

## Structure
This project is structured with the following structure.
``` text
.
├── build                       build directory
│   ├── unix                    linux build directory
│   └── win                     windows build directory
├── src                         source directory
│   └── main.cpp
├── password-generator          linux executable
├── password-generator.exe      windows executable
├── LICENSE 
├── Makefile
└── README
```

The build folder is used during the compilation process, it store the object file(.o) and the dependency file (.d) used by the compiler. The build folder has two sub folders, one to store the compilation files for linux executable and other for the windows directory.

The src folder is used to store the source code of the project, this include all source files (.cpp) and all header files (.h) for this project.

The linux and windows executable are created using the Makefile and their names are automatically set based on the project folder name.

## Building

Because this project can be builded for Linux and Windows, there are two ways of building this project:
### Linux
The following command is to build for linux:
```shell
    make linux
```
### Windows
The following command is to build for windows:
```shell
    make windows
```

## Debug

As recommended by the GNU Makefile documentation, the source files are always compile with the debug flag '-g'.

### VSCode

It's also possible to debug this project using vscode tools. All vscode configuration files are inside the folder .vscode.

A few extensions are need to debug this project with vscode. The vscode should ask if you want to install the extensions automatically but in case it doesn't just open the file .vscode/extensions.json and install all extensions present in the recommendations array.

To debug just press F5 to start the debugger and Ctrl+F5 to build and launch without debugging.

## Install

To install this project, use the make install-* command, this will build the project and copy the executable to the proper directory so this executable can be used from everywhere. 

### Linux

```shell
    make install-linux
```

### Windows **[WIP]**

## Release

To build a version of this project with static libraries, use the make with the release-* command.

### Linux

```shell
    make release-linux
```

### Windows

```shell
    make release-windows
```

## Run

### Linux

```shell
    ./password-generator
```

### Windows

```shell
    ./password-generator.exe
```
Wine can be used here to run the windows executable on linux using wine.

```shell
    wine password-generator.exe
```

## Usage

```shell
    ./password-generator --help
```

## Contributing

Please read [CONTRIBUTING.md](./CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

This project use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](REPO-LINK). 

## License
[GNU GPLv3](./LICENSE)